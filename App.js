import React from 'react';
import type {Node} from 'react';
import Router from './src/routers';

const App: () => Node = () => {
  return <Router />;
};

export default App;
