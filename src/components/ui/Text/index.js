import styled from 'styled-components';
import {colors} from '../../../utils/themes';
import metrics from '../../../utils/metrics';

export default styled.Text`
  color: ${({color}) => (color ? colors[color] : colors.blue500)};
  font-size: ${({size}) => (size ? metrics[`px${size}`] : metrics.px16)};
  weight: ${({weight}) => (weight ? metrics[`px${weight}`] : metrics.px16)};
`;
