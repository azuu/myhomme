import styled from 'styled-components';
import {colors} from '../../utils/themes';

export const Screen = styled.View`
  background: ${colors.blue500};
  flex: 1;
  justify-content: center;
`;

export const Container = styled.View`
  flex: 1;
  padding: 48px 40px;
`;

export const WrapperHeader = styled.SafeAreaView`
  flex: 1;
`;

export const WrapperBody = styled.SafeAreaView`
  width: 70%;
`;

export const WrapperForm = styled.SafeAreaView`
  background: ${colors.white500};
  width: 100%;
  border-radius: 15px;
`;
export const Form = styled.View`
  padding: 48px 20px;
`;
