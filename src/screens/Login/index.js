import React from 'react';
import {SafeAreaView} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Screen, WrapperForm, WrapperHeader, Form, WrapperBody, Container} from './styles';
import Logo  from '../../assets/logos/full-logo.svg'
import {Text} from '../../components/ui';

const Login = ({navigation}) => {
  return (
    <Screen>
      <Container>
        <WrapperHeader>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Logo />
          </TouchableOpacity>
        </WrapperHeader>
        <WrapperBody>
          <TouchableOpacity onPress={() => alert('checado')}>
              <Text color="white500" size="20" weight="700">
                Viver bem é uma escolha que você pode fazer hoje
              </Text>
          </TouchableOpacity>
        </WrapperBody> 
      </Container>   
      <WrapperForm>
        <Form>
          <Text>Email</Text>
          <Text>Senha</Text>
          <Text>Logar</Text>
          <Text>Esqueceu sua senha?</Text>
        </Form>
      </WrapperForm>
    </Screen>
  );
};

export default Login;
