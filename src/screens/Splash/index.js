import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Screen, Footer, Header} from './styles';
import {Text} from '../../components/ui';
import Logo from '../../assets/logos/logo.svg';
import useHooks from './hooks';

const Splash = ({navigation}) => {
  useHooks();

  return (
    <Screen>
      <Header>
        <Text size={16} color="white500">
          <Logo />
        </Text>
      </Header>
      <Footer>
        <TouchableOpacity>
          <Text size={12} color="orange500">
            Go Login
          </Text>
        </TouchableOpacity>
      </Footer>
    </Screen>
  );
};

export default Splash;
