import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

export default () => {
  const {replace} = useNavigation();

  useEffect(() => {
    setTimeout(() => {
      replace('Login');
    }, 3000);
  }, []);

  return {};
};
