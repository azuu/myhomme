import styled from 'styled-components';
import {colors} from '../../utils/themes';

export const Screen = styled.SafeAreaView`
  background: ${colors.blue500};
  flex: 1;
  align-items: center;
`;
export const Footer = styled.View``;
export const Header = styled.View`
  background: ${colors.blue500};
  flex: 1;
  align-items: center;
  justify-content: center;
`;
